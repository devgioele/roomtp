package xyz.gioeledevitti.roomtp.teleportation;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;
import xyz.gioeledevitti.roomtp.tasks.TpCountdown;
import xyz.gioeledevitti.roomtp.teleportation.controls.TpControl;
import xyz.gioeledevitti.roomtp.teleportation.controls.TpControlDamage;
import xyz.gioeledevitti.roomtp.teleportation.controls.TpControlMovement;
import xyz.gioeledevitti.roomtp.utils.Permissions;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TpController {

    /**
     * How many milliseconds a player must stay without taking damage to be able to visit a room.
     */
    public static final long TIME_NO_DAMAGE = 120000;
    /**
     * How many seconds a player must stand still to be able to visit a room.
     */
    public static final int TIME_NO_MOVEMENT = 20;

    private final Plugin plugin;
    private final ProtobufManager protobufManager;

    public TpController(Plugin plugin, ProtobufManager protobufManager) {
        this.plugin = plugin;
        this.protobufManager = protobufManager;
    }

    public void startSequence(Player player, Location destination, String roomName) {
        LinkedHashSet<TpControl> startControls = Stream.of(
                new TpControlDamage(player, TIME_NO_DAMAGE, protobufManager)
        ).collect(Collectors.toCollection(LinkedHashSet::new));

        LinkedHashSet<TpControl> controls = new LinkedHashSet<>(startControls);
        controls.add(new TpControlMovement(player));
        TpCountdown countdown =
                new TpCountdown(protobufManager, player, destination, roomName, TIME_NO_MOVEMENT, controls);

        if (Permissions.canBypassToRoom(player, roomName))
            countdown.teleport();
        else if (!playerViolated(startControls))
            countdown.runTaskTimer(plugin, 0, 20);
    }

    private boolean playerViolated(Set<TpControl> controls) {
        for(TpControl control : controls)
            if(!control.isFine(true))
                return true;
        return false;
    }


}
