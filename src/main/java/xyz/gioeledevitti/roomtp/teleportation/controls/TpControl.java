package xyz.gioeledevitti.roomtp.teleportation.controls;

import org.bukkit.entity.Player;

public abstract class TpControl {

    protected final Player player;

    public TpControl(Player player) {
        this.player = player;
    }

    public abstract boolean isFine(boolean printMessage);

}
