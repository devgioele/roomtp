package xyz.gioeledevitti.roomtp.teleportation.controls;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;
import xyz.gioeledevitti.roomtp.logging.Messenger;
import xyz.gioeledevitti.roomtp.utils.PrettyPrinter;
import xyz.gioeledevitti.roomtp.utils.TimeUtil;

public class TpControlDamage extends TpControl {

    private final long timeNoDamage;
    private final ProtobufManager protobufManager;

    public TpControlDamage(Player player, long timeNoDamage, ProtobufManager protobufManager) {
        super(player);
        this.timeNoDamage = timeNoDamage;
        this.protobufManager = protobufManager;
    }

    @Override
    public boolean isFine(boolean printMessage) {
        // Check if the player took damage recently
        long lastDamageMsRelative = getLastDamageRelative(player);
        if (lastDamageMsRelative < timeNoDamage) {
            if (printMessage) {
                long remainingTime = TimeUtil.period(lastDamageMsRelative, timeNoDamage);
                Messenger.sendMsg(player,
                        ChatColor.YELLOW + "Cannot teleport, because you recently took damage. Wait another " +
                                ChatColor.AQUA + PrettyPrinter.printTime(remainingTime) + ChatColor.YELLOW + ".");
            }
            return false;
        }
        return true;
    }

    private long getLastDamageRelative(Player player) {
        long lastDamageMs = protobufManager.bridgePlayersPB.getTookDamage(player.getUniqueId().toString());
        return TimeUtil.period(System.currentTimeMillis(), lastDamageMs);
    }

}
