package xyz.gioeledevitti.roomtp.teleportation.controls;

import org.bukkit.entity.Player;
import xyz.gioeledevitti.roomtp.commons.TpLocation;

public class TpControlMovement extends TpControl {

    private final TpLocation initLocation;

    public TpControlMovement(Player player) {
        super(player);
        initLocation = new TpLocation(player.getLocation());
    }

    @Override
    public boolean isFine(boolean printMessage) {
        return initLocation.equals(player.getLocation());
    }

}
