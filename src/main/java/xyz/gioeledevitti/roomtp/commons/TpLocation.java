package xyz.gioeledevitti.roomtp.commons;

import org.bukkit.Server;
import org.bukkit.World;
import xyz.gioeledevitti.roomtp.datastorage.proto.PlayersPB;
import xyz.gioeledevitti.roomtp.exceptions.WorldNotFoundException;

import java.util.Objects;

public class TpLocation extends org.bukkit.Location {

    private PlayersPB.Location protoLocation;

    /**
     * @return A new instance of {@link TpLocation}.
     * {@code null} if the location does not exist on the given server.
     */
    public static TpLocation fromProto(PlayersPB.Location protoLocation, Server server) throws WorldNotFoundException {
        String worldName = protoLocation.getWorld();
        World world = server.getWorld(worldName);
        if (world == null)
            throw new WorldNotFoundException(worldName);
        return new TpLocation(world, protoLocation.getX(), protoLocation.getY(), protoLocation.getZ());
    }

    public TpLocation(World world, double x, double y, double z) {
        super(world, x, y, z);
    }

    public TpLocation(World world, double x, double y, double z, float yaw, float pitch) {
        super(world, x, y, z, yaw, pitch);
    }

    public TpLocation(org.bukkit.Location location) {
        super(location.getWorld(), location.getX(), location.getY(), location.getZ(),
                location.getYaw(), location.getPitch());
    }

    public PlayersPB.Location toProto() {
        if (protoLocation == null) {
            protoLocation = PlayersPB.Location.newBuilder()
                    .setWorld(Objects.requireNonNull(getWorld()).getName())
                    .setX(getX())
                    .setY(getY())
                    .setZ(getZ())
                    .build();
        }
        return protoLocation;
    }

    public void applyYawPitchFrom(TpLocation other) {
        setYaw(other.getYaw());
        setPitch(other.getPitch());
    }

    public void applyYawPitchFrom(org.bukkit.Location other) {
        setYaw(other.getYaw());
        setPitch(other.getPitch());
    }

    /**
     * The same as {@link org.bukkit.Location#equals(Object)}, but ignores class type, yaw and pitch.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        final org.bukkit.Location other = (org.bukkit.Location) obj;
        if (getWorld() != other.getWorld() && (getWorld() == null || !getWorld().equals(other.getWorld())))
            return false;
        if (Double.doubleToLongBits(getX()) != Double.doubleToLongBits(other.getX()))
            return false;
        if (Double.doubleToLongBits(getY()) != Double.doubleToLongBits(other.getY()))
            return false;
        if (Double.doubleToLongBits(getZ()) != Double.doubleToLongBits(other.getZ()))
            return false;
        return true;
    }

}
