package xyz.gioeledevitti.roomtp.datastorage;

import java.io.File;

public abstract class ProtobufBridge {

    protected final File protobufFile;
    protected final String filename;

    protected ProtobufBridge(File directory, String filename) {
        this.filename = filename;
        protobufFile = new File(directory, filename);
    }

}
