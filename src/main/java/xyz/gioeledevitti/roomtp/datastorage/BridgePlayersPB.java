package xyz.gioeledevitti.roomtp.datastorage;

import org.apache.commons.io.FileUtils;
import xyz.gioeledevitti.roomtp.datastorage.proto.PlayersPB;
import xyz.gioeledevitti.roomtp.exceptions.PlayerNotFoundException;
import xyz.gioeledevitti.roomtp.logging.Messenger;
import xyz.gioeledevitti.roomtp.utils.Locations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

// TODO: Make thread-safe.
public class BridgePlayersPB extends ProtobufBridge {

    private static final String FILENAME = "players.pb";

    private PlayersPB.PlayersRecord cache;
    private PlayersPB.PlayersRecord.Builder builder;

    public BridgePlayersPB(File directory) {
        super(directory, FILENAME);
    }

    private PlayersPB.PlayersRecord getCache() {
        return cache;
    }

    private PlayersPB.PlayersRecord.Builder getNewBuilder() {
        builder = cache.toBuilder();
        return builder;
    }

    private void applyChanges() {
        cache = builder.build();
    }

    public void save() {
        try {
            try (FileOutputStream fos = FileUtils.openOutputStream(protobufFile)) {
                cache.writeTo(fos);
            }
        } catch (IOException e) {
            Messenger.log(Level.SEVERE, "Could not write to file " + filename + "!", e);
        }
    }

    /**
     * Reads the file's content and loads it in the cache.
     */
    public void load() {
        boolean exception = false;

        try {
            cache = PlayersPB.PlayersRecord.parseFrom(FileUtils.openInputStream(protobufFile));
        } catch (FileNotFoundException e) {
            exception = true;
            Messenger.log(Level.INFO, "File " + filename + " not found!");
        } catch (IOException e) {
            exception = true;
            Messenger.log(Level.WARNING, "Could not read file " + filename + "!");
        }
        if (exception) {
            Messenger.log(Level.INFO, "Loading default values.");
            cache = PlayersPB.PlayersRecord.getDefaultInstance();
        }
    }

    public void setPreviousLocation(String uuid, PlayersPB.Location previousLoc) {
        PlayersPB.Player.Builder playerBuilder;
        int index = -1;

        // Get player or create a new one
        try {
            index = indexOf(uuid);
            // Get builder from existing player
            playerBuilder = getCache().getPlayers(index).toBuilder();
        } catch (PlayerNotFoundException e) {
            // Create new player builder
            playerBuilder = PlayersPB.Player.newBuilder().setUuid(uuid);
        }

        // Set previous location if no one is already set
        if (!Locations.isValid(playerBuilder.getPreviousLocation())) {
            PlayersPB.Player playerChanged = playerBuilder.setPreviousLocation(previousLoc).build();

            if (index == -1) {
                // Add new player entry
                getNewBuilder().addPlayers(playerChanged);
            } else {
                // Set new value for already existing player entry
                getNewBuilder().setPlayers(index, playerChanged);
            }

            applyChanges();
        }
    }

    /**
     * @return The location where the player was before calling '/goto'.
     * {@code null} if the location has some values that are not set. This means that the player is not in any room.
     */
    public PlayersPB.Location getPreviousLocation(String uuid) {
        // Search for player
        int index = -1;
        try {
            index = indexOf(uuid);
        } catch (PlayerNotFoundException e) {
            return null;
        }
        // Get previous location
        PlayersPB.Location previousLoc = getCache().getPlayers(index).getPreviousLocation();
        // Verify that the location has values set
        if (Locations.isValid(previousLoc))
            return previousLoc;
        return null;
    }

    public void kick(String uuid) {
        int index;
        try {
            index = indexOf(uuid);
        } catch (PlayerNotFoundException e) {
            // Player could not be found, nothing to remove
            return;
        }

        PlayersPB.Player.Builder playerBuilder = getCache().getPlayers(index).toBuilder();
        // Remove 'previousLocation'
        PlayersPB.Player playerChanged = playerBuilder.clearPreviousLocation().build();
        getNewBuilder().setPlayers(index, playerChanged);

        applyChanges();
    }

    /**
     * Store if the player took damage, but only if the player is not in a room.
     *
     * @param uuid The UUID of the player that took damage.
     * @param time The time when the player took damage expressed as milliseconds after the UNIX epoch.
     */
    public void setTookDamage(String uuid, long time) {
        boolean isInRoom = false, notFound = false;
        int index = -1;
        PlayersPB.Player.Builder playerBuilder;

        // Search for the player
        try {
            index = indexOf(uuid);
            PlayersPB.Player player = getCache().getPlayers(index);
            playerBuilder = player.toBuilder();
            // If player is not in a room
            if (Locations.isValid(player.getPreviousLocation()))
                isInRoom = true;
        } catch (PlayerNotFoundException e) {
            playerBuilder = PlayersPB.Player.newBuilder();
            notFound = true;
        }

        if (!isInRoom) {
            // Set 'lastDamageMs'
            PlayersPB.Player playerChanged = playerBuilder.setUuid(uuid).setLastDamageMs(time).build();
            if (notFound)
                getNewBuilder().addPlayers(playerChanged);
            else
                getNewBuilder().setPlayers(index, playerChanged);

            applyChanges();
        }
    }

    /**
     * @return The UNIX timestamp for when the player took damage for the last time.
     * {@code -1} if the player could not be found.
     */
    public long getTookDamage(String uuid) {
        int index;

        try {
            index = indexOf(uuid);
        } catch (PlayerNotFoundException e) {
            return -1;
        }

        return getCache().getPlayers(index).getLastDamageMs();
    }

    private int indexOf(String playerUuid) throws PlayerNotFoundException {
        List<PlayersPB.Player> players = getCache().getPlayersList();

        for (int i = 0; i < players.size(); i++)
            if (players.get(i).getUuid().equals(playerUuid))
                return i;

        throw new PlayerNotFoundException(playerUuid);
    }

}
