package xyz.gioeledevitti.roomtp.datastorage;

import xyz.gioeledevitti.roomtp.logging.Messenger;

import java.io.File;

public class ProtobufManager {

    private static final String SUBDIRECTORY = "data";

    public final BridgePlayersPB bridgePlayersPB;
    public final BridgeRoomsPB bridgeRoomsPB;

    public ProtobufManager(File pluginDataFolder) {
        File protobufDirectory = pluginDataFolder.toPath()
                .resolve(SUBDIRECTORY).toFile();
        bridgePlayersPB = new BridgePlayersPB(protobufDirectory);
        bridgeRoomsPB = new BridgeRoomsPB(protobufDirectory);
    }
    
    public void load() {
        Messenger.log("Loading room data...");
        bridgeRoomsPB.load();
        Messenger.log("Loading player data...");
        bridgePlayersPB.load();
    }

    public void save() {
        Messenger.log("Saving player data...");
        bridgePlayersPB.save();
    }

}
