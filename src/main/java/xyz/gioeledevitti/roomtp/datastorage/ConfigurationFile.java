package xyz.gioeledevitti.roomtp.datastorage;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.gioeledevitti.roomtp.logging.Messenger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ConfigurationFile {

    private static final String FILENAME = "config.yml";
    private final File file;
    private final Plugin plugin;
    private YamlConfiguration content;

    public ConfigurationFile(JavaPlugin plugin) {
        this.file = new File(plugin.getDataFolder(), FILENAME);
        this.plugin = plugin;
    }

    private File getFile() {
        return file;
    }

    // TODO: Verify functionality and consistency.
    @Deprecated()
    public void load() {
        Messenger.log("Loading configuration file...");

        // Copy default version of the file if it doesn't exist
        if (!file.exists())
            plugin.saveResource(file.getName(), false);

        // Load file
        content = YamlConfiguration.loadConfiguration(file);

        // Check if there is embedded a default version of the file
        InputStream embeddedDefaultAsStream = getClass().getResourceAsStream(file.getName());
        if (embeddedDefaultAsStream != null)
            // Set source of all default values
            content.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(embeddedDefaultAsStream)));
    }

    /**
     * @throws IOException Thrown when the given file cannot be written to for any reason.
     */
    // TODO: Verify functionality and consistency.
    @Deprecated()
    public void save() throws IOException {
        content.save(file);
    }

    @Deprecated()
    public YamlConfiguration getContent() {
        return content;
    }

}
