package xyz.gioeledevitti.roomtp.datastorage;

import org.apache.commons.io.FileUtils;
import xyz.gioeledevitti.roomtp.datastorage.proto.PlayersPB;
import xyz.gioeledevitti.roomtp.datastorage.proto.RoomsPB;
import xyz.gioeledevitti.roomtp.exceptions.RoomAlreadyExistsException;
import xyz.gioeledevitti.roomtp.exceptions.RoomNotFoundException;
import xyz.gioeledevitti.roomtp.logging.Messenger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

// TODO: Make thread-safe.
public class BridgeRoomsPB extends ProtobufBridge {

    private static final String FILENAME = "rooms.pb";

    private RoomsPB.RoomsRecord cache;
    private RoomsPB.RoomsRecord.Builder builder;

    public BridgeRoomsPB(File directory) {
        super(directory, FILENAME);
    }

    private RoomsPB.RoomsRecord getCache() {
        return cache;
    }

    private RoomsPB.RoomsRecord.Builder getNewBuilder() {
        builder = cache.toBuilder();
        return builder;
    }

    private void applyChanges() {
        cache = builder.build();
    }

    public void save() {
        try {
            try (FileOutputStream fos = FileUtils.openOutputStream(protobufFile)) {
                cache.writeTo(fos);
            }
        } catch (IOException e) {
            Messenger.log(Level.SEVERE, "Could not write to file " + filename + "!", e);
        }
    }

    /**
     * Reads the file's content and loads it in the cache.
     */
    public void load() {
        boolean exception = false;

        try {
            cache = RoomsPB.RoomsRecord.parseFrom(FileUtils.openInputStream(protobufFile));
        } catch (FileNotFoundException e) {
            exception = true;
            Messenger.log(Level.INFO, "File " + filename + " not found!");
        } catch (IOException e) {
            exception = true;
            Messenger.log(Level.WARNING, "Could not read file " + filename + "!");
        }
        if (exception) {
            Messenger.log(Level.INFO, "Loading default values.");
            cache = RoomsPB.RoomsRecord.getDefaultInstance();
        }
    }

    public RoomsPB.Room getRoomByName(String roomName) throws RoomNotFoundException {
        return getCache().getRoomsList().get(indexOf(roomName));
    }

    public boolean roomExists(String roomName) {
        return getCache().getRoomsList().stream()
                .map(RoomsPB.Room::getName)
                .anyMatch(s -> s.equals(roomName));
    }

    public List<RoomsPB.Room> getRooms() {
        return getCache().getRoomsList();
    }

    public synchronized void addRoom(String roomName, PlayersPB.Location location) throws RoomAlreadyExistsException {
        if (roomExists(roomName))
            throw new RoomAlreadyExistsException(roomName);

        RoomsPB.Room newRoom = RoomsPB.Room.newBuilder()
                .setName(roomName)
                .setLocation(location)
                .build();
        getNewBuilder().addRooms(newRoom);

        applyChanges();
        save();
    }

    public synchronized void removeRoom(String roomName) throws RoomNotFoundException {
        // Try removing the room
        getNewBuilder().removeRooms(indexOf(roomName));

        applyChanges();
        save();
    }

    private int indexOf(String roomName) throws RoomNotFoundException {
        List<RoomsPB.Room> rooms = getCache().getRoomsList();

        for (int i = 0; i < rooms.size(); i++)
            if (rooms.get(i).getName().equals(roomName))
                return i;

        throw new RoomNotFoundException(roomName);
    }

}
