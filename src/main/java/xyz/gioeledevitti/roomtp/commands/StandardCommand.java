package xyz.gioeledevitti.roomtp.commands;

import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.Plugin;
import xyz.gioeledevitti.roomtp.datastorage.ConfigurationFile;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;

public abstract class StandardCommand implements CommandExecutor {

    protected final Plugin plugin;
    protected final ConfigurationFile config;
    protected final ProtobufManager protobufManager;

    public StandardCommand(Plugin plugin, ConfigurationFile config, ProtobufManager protobufManager) {
        this.plugin = plugin;
        this.config = config;
        this.protobufManager = protobufManager;
    }

}
