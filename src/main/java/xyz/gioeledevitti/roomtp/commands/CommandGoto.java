package xyz.gioeledevitti.roomtp.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import xyz.gioeledevitti.roomtp.commons.TpLocation;
import xyz.gioeledevitti.roomtp.teleportation.TpController;
import xyz.gioeledevitti.roomtp.datastorage.ConfigurationFile;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;
import xyz.gioeledevitti.roomtp.datastorage.proto.PlayersPB;
import xyz.gioeledevitti.roomtp.exceptions.RoomNotFoundException;
import xyz.gioeledevitti.roomtp.exceptions.WorldNotFoundException;
import xyz.gioeledevitti.roomtp.logging.Message;
import xyz.gioeledevitti.roomtp.logging.Messenger;
import xyz.gioeledevitti.roomtp.utils.Permissions;

import java.util.logging.Level;

public class CommandGoto extends StandardCommand {

    private final TpController tpController;

    public CommandGoto(Plugin plugin, ConfigurationFile config, ProtobufManager protobufManager) {
        super(plugin, config, protobufManager);
        tpController = new TpController(plugin, protobufManager);
    }

    /**
     * Executes the given command, returning its success.
     * <br>
     * If false is returned, then the "usage" plugin.yml entry for this command
     * (if defined) will be sent to the player.
     *
     * @param sender  Source of the command
     * @param command Command which was executed
     * @param label   Alias of the command which was used
     * @param args    Passed command arguments
     * @return true if a valid command, otherwise false
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length == 1) {
                String roomName = args[0];
                if (Permissions.canAccessRoom(sender, roomName)) {
                    tryPlayerToRoom((Player) sender, roomName);
                } else {
                    Messenger.sendMsg(sender, Message.CANNOT_ACCESS_ROOM);
                    Messenger.sendMsg(sender, Message.INSUFFICIENT_PERMISSION);
                }
            } else
                return false;
        } else
            Messenger.sendMsg(sender, Message.ONLY_PLAYERS_CAN_USE);
        return true;
    }

    private void tryPlayerToRoom(Player player, String roomName) {
        PlayersPB.Location protoDestination;
        Location destination;

        try {
            protoDestination = protobufManager.bridgeRoomsPB.getRoomByName(roomName).getLocation();
        } catch (RoomNotFoundException e) {
            Messenger.sendMsg(player, Message.ROOM_NOT_FOUND);
            return;
        }

        try {
            destination = TpLocation.fromProto(protoDestination, player.getServer());
        } catch(WorldNotFoundException e) {
            Messenger.log(Level.WARNING, Message.TELEPORTATON_FAILED, e);
            Messenger.sendMsg(player, Message.TELEPORTATON_FAILED, e);
            return;
        }

        tpController.startSequence(player, destination, roomName);
    }

}
