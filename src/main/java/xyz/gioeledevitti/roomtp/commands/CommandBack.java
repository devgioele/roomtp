package xyz.gioeledevitti.roomtp.commands;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import xyz.gioeledevitti.roomtp.commons.TpLocation;
import xyz.gioeledevitti.roomtp.datastorage.ConfigurationFile;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;
import xyz.gioeledevitti.roomtp.datastorage.proto.PlayersPB;
import xyz.gioeledevitti.roomtp.exceptions.WorldNotFoundException;
import xyz.gioeledevitti.roomtp.logging.Message;
import xyz.gioeledevitti.roomtp.logging.Messenger;
import xyz.gioeledevitti.roomtp.utils.Permissions;

import java.util.logging.Level;

public class CommandBack extends StandardCommand {

    public CommandBack(Plugin plugin, ConfigurationFile config, ProtobufManager protobufManager) {
        super(plugin, config, protobufManager);
    }

    /**
     * Executes the given command, returning its success.
     * <br>
     * If false is returned, then the "usage" plugin.yml entry for this command
     * (if defined) will be sent to the player.
     *
     * @param sender  Source of the command
     * @param command Command which was executed
     * @param label   Alias of the command which was used
     * @param args    Passed command arguments
     * @return true if a valid command, otherwise false
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length == 0) {
                if (Permissions.canUseBack(sender))
                    playerBack((Player) sender);
                else
                    Messenger.sendMsg(sender, Message.INSUFFICIENT_PERMISSION);
            } else
                return false;
        } else
            Messenger.sendMsg(sender, Message.ONLY_PLAYERS_CAN_USE);
        return true;
    }

    private void playerBack(Player player) {
        // Get the previous location of the player
        TpLocation previousLoc;
        PlayersPB.Location previousLocProto =
                protobufManager.bridgePlayersPB.getPreviousLocation(player.getUniqueId().toString());

        if (previousLocProto == null) {
            Messenger.sendMsg(player, Message.NOT_IN_ROOM);
        } else {
            try {
                previousLoc = TpLocation.fromProto(previousLocProto, plugin.getServer());
                previousLoc.applyYawPitchFrom(player.getLocation());
            } catch (WorldNotFoundException e) {
                Messenger.log(Level.SEVERE, Message.LOCATION_NOT_FOUND);
                Messenger.sendMsg(player, Message.LOCATION_NOT_FOUND);
                return;
            }

            // Teleport player
            player.teleport(previousLoc);
            protobufManager.bridgePlayersPB.kick(player.getUniqueId().toString());
            Messenger.broadcast(ChatColor.BLUE + player.getName() + " left a room!");
        }
    }

}
