package xyz.gioeledevitti.roomtp.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import xyz.gioeledevitti.roomtp.commons.TpLocation;
import xyz.gioeledevitti.roomtp.datastorage.ConfigurationFile;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;
import xyz.gioeledevitti.roomtp.datastorage.proto.RoomsPB;
import xyz.gioeledevitti.roomtp.exceptions.RoomAlreadyExistsException;
import xyz.gioeledevitti.roomtp.exceptions.RoomNotFoundException;
import xyz.gioeledevitti.roomtp.logging.Message;
import xyz.gioeledevitti.roomtp.logging.Messenger;
import xyz.gioeledevitti.roomtp.utils.Permissions;
import xyz.gioeledevitti.roomtp.utils.PrettyPrinter;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommandRoomTP extends StandardCommand {

    public CommandRoomTP(Plugin plugin, ConfigurationFile config, ProtobufManager protobufManager) {
        super(plugin, config, protobufManager);
    }

    /**
     * Executes the given command, returning its success.
     * <br>
     * If false is returned, then the "usage" plugin.yml entry for this command
     * (if defined) will be sent to the player.
     *
     * @param sender  Source of the command
     * @param command Command which was executed
     * @param label   Alias of the command which was used
     * @param args    Passed command arguments
     * @return true if a valid command, otherwise false
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            // We are talking about 'help', 'reload' or 'list'
            if (args[0].equals("help")) {
                Messenger.sendMsg(sender, Message.HELP);
            } else if (args[0].equals("reload")) {
                config.load();
            } else if (args[0].equals("list")) {
                listRooms(sender);
            } else
                return false;

        } else if (args.length == 2) {
            if (sender instanceof Player) {
                if(sender.hasPermission("roomtp.modify")) {
                    String roomName = args[1];
                    switch (args[0]) {
                        case "add":
                            tryAddRoom(sender, roomName);
                            break;
                        case "remove":
                            tryRemoveRoom(sender, roomName);
                            break;
                        case "kick":
                            tryKick(sender, args[1]);
                            break;
                        default:
                            return false;
                    }
                } else
                    Messenger.sendMsg(sender, Message.INSUFFICIENT_PERMISSION);
            } else
                Messenger.sendMsg(sender, Message.ONLY_PLAYERS_CAN_USE);

        } else
            return false;
        return true;
    }

    private void tryKick(CommandSender sender, String playernameToKick) {
        Player playerToKick = plugin.getServer().getPlayer(playernameToKick);
        if (playerToKick == null) {
            Messenger.sendMsg(sender,Message.PLAYER_NOT_FOUND);
        } else {
            protobufManager.bridgePlayersPB.kick(playerToKick.getUniqueId().toString());
            Messenger.sendMsg(sender, ChatColor.GOLD + playernameToKick + ChatColor.GRAY + " is no longer in any room.");
        }
    }

    private void listRooms(CommandSender sender) {
        List<RoomsPB.Room> roomsList = protobufManager.bridgeRoomsPB.getRooms();
        // Get room names and sort them alphabetically
        Collection<String> roomNames =
                roomsList.stream()
                        .filter(r -> Permissions.canAccessRoom(sender, r))
                        // Sort according to name
                        .sorted(Comparator.comparing(RoomsPB.Room::getName))
                        // Map to names and locations
                        .map(r -> "\n" + ChatColor.GOLD + r.getName() + "\n" + ChatColor.GRAY +
                                PrettyPrinter.printLoc(r.getLocation()))
                        .collect(Collectors.toList());
        // Format message
        String msg = "Rooms available:\n" + String.join("\n", roomNames);
        Messenger.sendMsg(sender, msg);
    }

    private void tryAddRoom(CommandSender sender, String roomName) {
        TpLocation playerLoc = new TpLocation(((Player) sender).getLocation());
        try {
            protobufManager.bridgeRoomsPB.addRoom(roomName, playerLoc.toProto());
            Messenger.sendMsg(sender, ChatColor.GREEN + "Room '" + roomName + "' registered!\n"
                    + PrettyPrinter.printLoc(playerLoc));
        } catch (RoomAlreadyExistsException e) {
            Messenger.sendMsg(sender, Message.ROOM_ALREADY_EXISTS, e);
        }
    }

    private void tryRemoveRoom(CommandSender sender, String roomName) {
        try {
            protobufManager.bridgeRoomsPB.removeRoom(roomName);
            Messenger.sendMsg(sender,
                    ChatColor.GREEN + "Room '" + roomName + "' deleted!");
        } catch (RoomNotFoundException e) {
            Messenger.sendMsg(sender, Message.ROOM_NOT_FOUND);
        }
    }

}
