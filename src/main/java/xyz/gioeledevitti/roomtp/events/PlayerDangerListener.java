package xyz.gioeledevitti.roomtp.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;

public class PlayerDangerListener extends StandardListener {

    public PlayerDangerListener(ProtobufManager protobufManager) {
        super(protobufManager);
    }

    @EventHandler(ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player && event.getFinalDamage() > 0) {
            Player player = (Player) event.getEntity();
            protobufManager.bridgePlayersPB.setTookDamage(player.getUniqueId().toString(), System.currentTimeMillis());
        }
    }

}