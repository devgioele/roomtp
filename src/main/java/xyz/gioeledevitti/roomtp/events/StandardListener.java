package xyz.gioeledevitti.roomtp.events;

import org.bukkit.event.Listener;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;

public abstract class StandardListener implements Listener {

    protected final ProtobufManager protobufManager;

    public StandardListener(ProtobufManager protobufManager) {
        this.protobufManager = protobufManager;
    }

}
