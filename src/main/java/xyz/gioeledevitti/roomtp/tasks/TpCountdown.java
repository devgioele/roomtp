package xyz.gioeledevitti.roomtp.tasks;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import xyz.gioeledevitti.roomtp.teleportation.controls.TpControl;
import xyz.gioeledevitti.roomtp.commons.TpLocation;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;
import xyz.gioeledevitti.roomtp.logging.Messenger;
import xyz.gioeledevitti.roomtp.utils.PrettyPrinter;

import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public class TpCountdown extends BukkitRunnable {

    private final ProtobufManager protobufManager;
    private final Player player;
    private final TpLocation destination;
    private final TpLocation initLocation;
    private final String roomName;
    private final HashSet<TpControl> tpControls;

    private int counter;

    public TpCountdown(ProtobufManager protobufManager, Player player, Location destination,
                       String roomName, int counter, HashSet<TpControl> tpControls) {
        if (counter <= 0)
            throw new IllegalArgumentException("Counter must start greater than 0");

        this.protobufManager = protobufManager;
        this.player = player;
        this.destination = new TpLocation(destination);
        this.roomName = roomName;
        this.counter = counter;
        this.tpControls = tpControls;

        // Save initial location and apply yaw & pitch to destination
        initLocation = new TpLocation(player.getLocation());
        this.destination.applyYawPitchFrom(initLocation);
    }

    /**
     * Send message to player with variable interval, depending on counter progress.
     */
    @Override
    public void run() {
        if(playerViolated()) {
            Messenger.sendMsg(player, ChatColor.RED + "Teleportation cancelled.");
            this.cancel();
        } else if (counter == 0) {
            // Teleport and stop
            teleport();
            this.cancel();
        } else {
            if (intervalElapsed()) {
                Messenger.sendMsg(player,
                        ChatColor.YELLOW + "Teleporting in " + ChatColor.AQUA +
                                PrettyPrinter.printTime(counter, TimeUnit.SECONDS) + ChatColor.YELLOW + "...");
            }
        }
        counter--;
    }

    private boolean intervalElapsed() {
        int interval;

        // Establish interval
        if (counter > 30)
            // From 30 to infinity
            interval = 10;
        else if (counter > 15)
            // From 16 to 30
            interval = 5;
        else
            // From 1 to 15
            interval = 1;

        return counter % interval == 0;
    }

    public void teleport() {
        player.teleport(destination);
        // Register that the player is in a room
        protobufManager.bridgePlayersPB.setPreviousLocation(player.getUniqueId().toString(), initLocation.toProto());
        Messenger.broadcast(ChatColor.BLUE + player.getName() + " entered the " +
                ChatColor.GOLD + roomName + ChatColor.BLUE + " room!");
    }

    private boolean playerViolated() {
        for(TpControl control : tpControls)
            if(!control.isFine(false))
                return true;
        return false;
    }

}
