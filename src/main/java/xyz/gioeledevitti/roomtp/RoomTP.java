package xyz.gioeledevitti.roomtp;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.gioeledevitti.roomtp.commands.CommandBack;
import xyz.gioeledevitti.roomtp.commands.CommandGoto;
import xyz.gioeledevitti.roomtp.commands.CommandRoomTP;
import xyz.gioeledevitti.roomtp.datastorage.ConfigurationFile;
import xyz.gioeledevitti.roomtp.datastorage.ProtobufManager;
import xyz.gioeledevitti.roomtp.events.PlayerDangerListener;
import xyz.gioeledevitti.roomtp.logging.Messenger;

import java.util.Objects;

public final class RoomTP extends JavaPlugin {

    private ProtobufManager protobufManager;
    private ConfigurationFile config;

    /**
     * Plugin startup logic.
     * Is called when the server enable this plugin.
     */
    @Override
    public void onEnable() {
        Messenger.setup(this);

        config = new ConfigurationFile(this);
        // TODO: config.load();
        protobufManager = new ProtobufManager(getDataFolder());
        protobufManager.load();
        registerCommands();
        registerEventListeners();

        Messenger.log("Enabled successfully!");
    }

    /**
     * Plugin shutdown logic.
     * Is called when the server disables this plugin.
     */
    @Override
    public void onDisable() {
        protobufManager.save();
        HandlerList.unregisterAll(this);

        Messenger.log("Disabled successfully!");
    }

    private void registerCommands() {
        Objects.requireNonNull(getCommand("roomtp")).setExecutor(new CommandRoomTP(this, config, protobufManager));
        Objects.requireNonNull(getCommand("goto")).setExecutor(new CommandGoto(this, config, protobufManager));
        Objects.requireNonNull(getCommand("back")).setExecutor(new CommandBack(this, config, protobufManager));
    }

    private void registerEventListeners() {
        getServer().getPluginManager().registerEvents(new PlayerDangerListener(protobufManager), this);
    }

}