package xyz.gioeledevitti.roomtp.logging;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Messenger {

    private static Plugin plugin;
    private static Logger logger;
    private static String prefix;

    public static void setup(Plugin plugin) {
        Messenger.plugin = plugin;
        logger = plugin.getLogger();
        String plainPrefix = plugin.getDescription().getPrefix();
        prefix = ChatColor.BOLD + "" + ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + plainPrefix
                + ChatColor.DARK_GRAY + "] " + ChatColor.RESET + "" + ChatColor.GRAY;
    }

    public static void broadcast(String msg) {
        plugin.getServer().broadcastMessage(prefix + msg);
    }

    public static void log(String msg) {
        log(Level.INFO, msg);
    }

    public static void log(Level level, String msg) {
        logger.log(level, msg);
    }

    public static void log(Level level, Message msg) {
        logger.log(level, msg.toString());
    }

    public static void log(Level level, String msg, Throwable thrown) {
        logger.log(level, msg, thrown);
    }

    public static void log(Level level, Message msg, Throwable thrown) {
        log(level, msg.toString(), thrown);
    }

    public static void sendMsg(CommandSender receiver, Message msg) {
        receiver.sendMessage(prefix + msg);
    }

    public static void sendMsg(CommandSender receiver, String msg) {
        receiver.sendMessage(prefix + msg);
    }

    public static void sendMsg(CommandSender receiver, String msg, Throwable thrown) {
        receiver.sendMessage(prefix + msg + " " + thrown.getMessage());
    }

    public static void sendMsg(CommandSender receiver, Message msg, Throwable thrown) {
        sendMsg(receiver, msg.toString(), thrown);
    }

}
