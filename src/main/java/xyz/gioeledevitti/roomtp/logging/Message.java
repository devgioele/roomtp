package xyz.gioeledevitti.roomtp.logging;

import org.bukkit.ChatColor;
import xyz.gioeledevitti.roomtp.datastorage.ConfigurationFile;

public enum Message {
    HELP(ChatColor.GRAY + "Commands available:\n\n" +
            ChatColor.GOLD + "/goto <roomname>" + ChatColor.GRAY + " - Teleports you to the given room.\n" +
            ChatColor.GOLD + "/back" + ChatColor.GRAY + " - Teleports you back to your previous location before " +
            "calling /goto.\n" +
            ChatColor.GOLD + "/roomtp add <roomname>" + ChatColor.GRAY + " - Creates a new room at the player's " +
            "current location.\n" +
            ChatColor.GOLD + "/roomtp remove <roomname>" + ChatColor.GRAY + " - Removes the given room.\n" +
            ChatColor.GOLD + "/roomtp list" + ChatColor.GRAY + " - Lists available rooms, according to your " +
            "permission.\n" +
            ChatColor.GOLD + "/roomtp reloadConfigFiles" + ChatColor.GRAY + " - Reloads the plugin.\n" +
            ChatColor.GOLD + "/roomtp help" + ChatColor.GRAY + " - Shows this help page.\n" +
            "\nEnd of help page."),
    ONLY_PLAYERS_CAN_USE(ChatColor.RED + "You must be a player to execute this command!"),
    ROOM_ALREADY_EXISTS(ChatColor.RED + "Cannot add room!"),
    ROOM_NOT_FOUND(ChatColor.RED + "This room does not exist!"),
    CANNOT_ACCESS_ROOM(ChatColor.RED + "You cannot acccess that room!"),
    TELEPORTATON_FAILED(ChatColor.RED + "Teleportation failed!"),
    INSUFFICIENT_PERMISSION(ChatColor.RED + "Insufficient permission."),
    NOT_IN_ROOM(ChatColor.RED + "You are not in a room, so you cannot go back!"),
    LOCATION_NOT_FOUND(ChatColor.RED + "'/back' could not be used, because the " +
            "previous location did no longer exist! Did the server administrator delete that world?"),
    PLAYER_NOT_FOUND(ChatColor.RED + "The player is offline or does not exist!");

    public static void loadFromConfig(ConfigurationFile config) {
        // TODO: Apply messages defined in the config file
        /*
        for each enumInstance
            if enumInstance isConfigurable()
                enumInstance.setMsg(config.getValueAtPath(enumInstance.getConfigPath()))
         */
    }

    private String configPath;
    private String msg;

    Message(String configPath, String msg) {
        this.configPath = configPath;
        this.msg = msg;
    }

    Message(String msg) {
        this.msg = msg;
    }

    private void setMsg(String msg) {
        this.msg = msg;
    }

    private String getConfigPath() {
        return configPath;
    }

    private boolean isConfigurable() {
        return configPath != null;
    }

    @Override
    public String toString() {
        return msg;
    }

}
