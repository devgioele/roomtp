package xyz.gioeledevitti.roomtp.utils;

public class TimeUtil {

    private TimeUtil() {}

    /**
     * @return In milliseconds the absolute difference.
     */
    public static long period(long time1, long time2) {
        if(time1 < time2)
            return time2 - time1;
        return time1 - time2;
    }

    public static long toSeconds(long millis) {
        return Math.round((double)millis/1000);
    }

}
