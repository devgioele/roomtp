package xyz.gioeledevitti.roomtp.utils;

import org.bukkit.Location;
import xyz.gioeledevitti.roomtp.datastorage.proto.PlayersPB;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class PrettyPrinter {

    private PrettyPrinter() {
    }

    public static String printLoc(Location loc) {
        return "WORLD = " + Objects.requireNonNull(loc.getWorld()).getName()
                + "\nX = " + String.format("%.2f", loc.getX())
                + "\nY = " + String.format("%.2f", loc.getY())
                + "\nZ = " + String.format("%.2f", loc.getZ());
    }

    public static String printLoc(PlayersPB.Location loc) {
        return "WORLD = " + loc.getWorld()
                + "\nX = " + String.format("%.2f", loc.getX())
                + "\nY = " + String.format("%.2f", loc.getY())
                + "\nZ = " + String.format("%.2f", loc.getZ());
    }

    public static String printTime(long periodMillis) {

        long periodSeconds = Math.round((double) periodMillis / 1000);
        Duration duration = Duration.ofSeconds(periodSeconds);

        return duration.toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }

    public static String printTime(long amount, TimeUnit unit) {
        return printTime(unit.toMillis(amount));
    }

}
