package xyz.gioeledevitti.roomtp.utils;

import xyz.gioeledevitti.roomtp.datastorage.proto.PlayersPB;

public final class Locations {

    private Locations() {
    }

    public static boolean isValid(PlayersPB.Location location) {
        return !location.getWorld().isEmpty();
    }

}
