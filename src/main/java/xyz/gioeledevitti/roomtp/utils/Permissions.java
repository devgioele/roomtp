package xyz.gioeledevitti.roomtp.utils;

import org.bukkit.command.CommandSender;
import xyz.gioeledevitti.roomtp.datastorage.proto.RoomsPB;

public class Permissions {

    private Permissions() {}

    public static boolean canUseBack(CommandSender sender) {
        return sender.hasPermission("roomtp.back");
    }

    public static boolean canAccessRoom(CommandSender sender, RoomsPB.Room room) {
        return canAccessRoom(sender, room.getName());
    }

    public static boolean canAccessRoom(CommandSender sender, String roomName) {
        return sender.hasPermission("roomtp.goto." + roomName)
                || sender.hasPermission("roomtp.goto.*");
    }

    public static boolean canBypassToRoom(CommandSender sender, String roomName) {
        return sender.hasPermission("roomtp.bypass." + roomName)
                || sender.hasPermission("roomtp.bypass.*");
    }

}
