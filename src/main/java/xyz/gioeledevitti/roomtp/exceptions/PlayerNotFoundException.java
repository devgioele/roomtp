package xyz.gioeledevitti.roomtp.exceptions;

public class PlayerNotFoundException extends Exception {

    private final String uuid;

    public PlayerNotFoundException(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getMessage() {
        return "Couldn't find player with UUID '"+uuid+"'!";
    }

}
