package xyz.gioeledevitti.roomtp.exceptions;

public class RoomAlreadyExistsException extends RoomException {

    public RoomAlreadyExistsException(String roomName) {
        super(roomName);
    }

    @Override
    public String getMessage() {
        return "Room '"+roomName+"' already exists!";
    }

}
