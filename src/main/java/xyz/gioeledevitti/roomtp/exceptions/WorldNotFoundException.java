package xyz.gioeledevitti.roomtp.exceptions;

public class WorldNotFoundException extends Exception {

    private final String worldName;

    public WorldNotFoundException(String worldName) {
        this.worldName = worldName;
    }

    @Override
    public String getMessage() {
        return "Could not find world '"+worldName+"'!";
    }

}
