package xyz.gioeledevitti.roomtp.exceptions;

public class RoomException extends Exception {

    protected final String roomName;

    public RoomException(String roomName) {
        this.roomName = roomName;
    }

}
