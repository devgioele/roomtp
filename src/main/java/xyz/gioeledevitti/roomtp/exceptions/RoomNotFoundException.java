package xyz.gioeledevitti.roomtp.exceptions;

public class RoomNotFoundException extends RoomException {

    public RoomNotFoundException(String roomName) {
        super(roomName);
    }

    @Override
    public String getMessage() {
        return "Room '"+roomName+"' could not be found!";
    }

}
